<?php $this->load->view('admin/components/page_head') ?>

<nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-between px-5">
	<a class="navbar-brand" href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-tint"></i> <?php echo $meta_title; ?></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse justify-content-between" id="navbarSupportedContent" >
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="<?php echo site_url('admin/dashboard'); ?>">Dashboard <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo site_url('admin/page'); ?>">Pages</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo site_url('admin/page/order'); ?>">Order Pages</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo site_url('admin/user'); ?>">Users</a>
			</li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item">
				<?php echo mailto('yansantana@ydesign.dev' , ' <i class="fa fa-user"></i> gwenstacy@gmail.com' , 'class="nav-link text-info"') ?>
			</li>
			<li class="nav-item">
				<?php echo anchor('admin/user/logout' , 'Logout', 'class="btn btn-outline-danger my-2 my-sm-0"') ?>
			</li>
		</ul>
	</div>
</nav>

<div class="container">
	<!-- Main Column -->
	<div class="row">
	<div class="col-9">
		<?php $this->load->view($subview); ?>
	</div>
	</div>
</div>

<?php $this->load->view('admin/components/page_tail') ?>




