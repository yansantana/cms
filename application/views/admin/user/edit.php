<div class="py-3">

	<h3><?php echo empty($user->id) ? '<i class="fa fa-plus"></i> Add a New User' : '<i class="fa fa-edit"></i> Edit User ' . $user->name  ?></h3>
	<?php echo validation_errors(); ?>
	<?php echo form_open(); ?>
	<form>
		<div class="form-group">
			<label for="exampleInputEmail1">Name</label>
			<?php echo form_input('name', set_value('name',$user->name) , 'class="form-control"') ?>
		</div>
		<div class="form-group">
			<label for="exampleInputEmail1">Email address</label>
			<?php echo form_input('email', set_value('email', $user->email),'class="form-control"') ?>
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Password</label>
			<?php echo form_password('password', '','class="form-control"') ?>
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Confirm Password</label>
			<?php echo form_password('password_confirm', '','class="form-control"') ?>
		</div>

		<?php echo form_submit('submit' , 'Save' , 'class="btn btn-primary"') ?>
	</form>
	<?php echo form_close(); ?>

</div>
