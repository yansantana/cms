<section>

<div class="row m-5">
	<h3><i class="fa fa-user"></i>  Users</h3>
	<?php echo anchor('admin/user/edit', ' <i class="fa fa-user-plus"></i> Add a User ', 'class="btn btn-success ml-3"'); ?>
</div>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<?php if($users): foreach($users as $user): ?>
			<tr>
				<td><?php echo anchor('admin/user/edit/' . $user->id , $user->name); ?></td>
				<td><?php echo anchor('admin/user/edit/' . $user->id , $user->email); ?></td>
				<td><?php echo btn_edit('admin/user/edit/'. $user->id) ?></td>
				<td><?php echo btn_delete('admin/user/delete/'. $user->id) ?></td>
			</tr>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td>No users found.</td>
				</tr>
			<?php endif;?>
		</tbody>
	</table>
</section>
