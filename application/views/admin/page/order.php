<section>
<div class=" m-5">
	<h2>Order Pages</h2>
	<p class="text-muted ml-4">Drag to order pages and then click 'Save'.</p>
	<div id="orderResult"></div>
	<input type="button" value="Save" id="save" class="btn btn-primary">
</div>
</section>

<script>
	$(function() {
		$.post('<?php echo site_url('admin/page/order_ajax'); ?>', {}, function(data) {
			$('#orderResult').html(data);
		});

		$('#save').click(function() {
			oSortable = $('.sortable').nestedSortable('toArray');

			$('#orderResult').slideUp(function(){
				$.post('<?php echo site_url('admin/page/order_ajax'); ?>' , { sortable: oSortable }, function(data) {
					$('#orderResult').html(data);
					$('#orderResult').slideDown();
				});
			});
		});
	});
</script>
