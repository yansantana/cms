<?php 
	echo get_ol($pages);

	function get_ol($array, $child = FALSE) {
		$str = '';

		if($array) {
			$str .= $child ==FALSE ? ' <ol class="sortable">' : '<ol> ';
			
			foreach ($array as $item) {
				$str .= ' <li id="list_' . $item['id'] .'">';
				$str .= '<div>' . $item['title'] . '</div>';

				//Children?

				if (isset($item['children']) && $item['children']) {
					$str .= get_ol($item['children'], TRUE);
				}

				$str .= '</li>' . PHP_EOL;
			}

			$str .= '</ol>' . PHP_EOL;
		}
		return $str;
	}
?>

<script>
	$(document).ready(function(){

		$('.sortable').nestedSortable({
			handle: 'div',
			items: 'li',
			toleranceElement: '> div',
			maxLevels: 2
		});

	});
</script>
