<div class="py-3">

<h3><?php echo empty($page->id) ? '<i class="fa fa-plus"></i> Add a New Page' : '<i class="fa fa-edit"></i> Edit Page ' . $page->title  ?></h3>

<?php echo validation_errors(); ?>
<?php echo form_open(); ?>
<form>
	<div class="form-group">
		<label for="exampleInputEmail1">Parent</label>
		<?php echo form_dropdown('parent_id', $pages_no_parents , $this->input->post('parent_id') ? $this->input->post('parent_id') : $page->parent_id , 'class="form-control"') ?>
	</div>
	<div class="form-group">
		<label for="exampleInputEmail1">Title</label>
		<?php echo form_input('title', set_value('title',$page->title) , 'class="form-control"') ?>
	</div>
	<div class="form-group">
		<label for="exampleInputEmail1">Slug</label>
		<?php echo form_input('slug', set_value('slug', $page->slug),'class="form-control"') ?>
	</div>
	<div class="form-group">
		<label for="exampleInputPassword1">Body</label>
		<?php echo form_textarea('body', set_value('body', $page->body),'class="form-control"') ?>
	</div>

	<?php echo form_submit('submit' , 'Save' , 'class="btn btn-primary"') ?>
</form>
<?php echo form_close(); ?>

</div>
