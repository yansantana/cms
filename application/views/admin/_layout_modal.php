<?php $this->load->view('admin/components/page_head') ?>


<div class="modal d-block" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<?php $this->load->view($subview); ?>
		<div class="modal-footer">
			&copy; <?php echo $meta_title; ?>
		</div>
	</div>
  </div>

<?php $this->load->view('admin/components/page_tail') ?>
